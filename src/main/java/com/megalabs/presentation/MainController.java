package com.megalabs.presentation;

import com.megalabs.presentation.helper.ModelAndViewHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Just lead to the main page
 */
@Controller
public class MainController {

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public ModelAndView getStartPage() {
        return ModelAndViewHelper.getModelAndView("mainPage");
    }

}
