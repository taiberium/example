package com.megalabs.presentation;

import com.megalabs.service.PyramidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Send JSON data, if any exception thrown, then GlobalDefaultExceptionHandler will deals with them
 */
@RestController
public class PyramidController {

    private PyramidService pyramidService;

    @Autowired
    public PyramidController(PyramidService pyramidService) {
        this.pyramidService = pyramidService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/weight", produces = MediaType.APPLICATION_JSON_VALUE)
    public double findWeightA(@RequestParam("level") Long level, @RequestParam("element") Long element) {
        return findWeight(level, element);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/weight/{level}/{element}", produces = MediaType.APPLICATION_JSON_VALUE)
    public double findWeightB(@PathVariable("level") Long level, @PathVariable("element") Long element) {
        return findWeight(level, element);
    }

    // Validation of input params
    private double findWeight(Long level, Long element) {
        if (
                level == null || Long.signum(level) == -1 ||
                element == null || Long.signum(element) == -1 ||
                pyramidService.findMaxPyramidLevel() < level ||
                pyramidService.findMaxElement(level) < element
                ) {
            throw new IllegalArgumentException("Level or element can't be like that");
        }
        return pyramidService.findWeightOnTopOfBlock(level, element);
    }
}
