<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="jumbotron">
  <div class="container">
    <h1 class="display-3">Calculate pressure!</h1>
    <form id="pyramidForm" method="get">
      <label for="level">Pyramid level: </label>
      <input style="width:230px" name="level" placeholder="Write pyramid level" class="form-control" id="level">
      <label for="element">Pyramid element: </label>
      <input style="width:230px" name="element" placeholder="Write pyramid level element" class="form-control" id="element">
    </form>
    <br/>
    <p><div class="btn btn-primary btn-lg" role="button" onclick="loadPressure()">Calculate pressure!</div></p>
    <br/>
    <div id="data"></div>
  </div>
</div>
