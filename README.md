** Main page**  
[![https://gyazo.com/3eaa69452050ad9f9fd73b3186ee7b24](https://i.gyazo.com/3eaa69452050ad9f9fd73b3186ee7b24.png)](https://gyazo.com/3eaa69452050ad9f9fd73b3186ee7b24)

** Error page **  
If any exception thrown, then we see that page:  
[![https://gyazo.com/af11044d3891d269aea244ae1607c8a4](https://i.gyazo.com/af11044d3891d269aea244ae1607c8a4.png)](https://gyazo.com/af11044d3891d269aea244ae1607c8a4)

** Info **  
In application.properties parameter server.tomcat.max-threads is set to 1000(default 200), so   
application can handle 1000 simultaneous client threads, and Tomcat NIO maxConnections  
the default is the value 10000 (can handle > 10^2 simultaneous connections);  

In PyramidServiceImplTest is shown, what 1 million worst scenario method calls  
are performed in a time less than 60ms. This method could be even called 10 million times  
and still it will be performed faster than half of second.(can handle > 10^3 calls per second);  

For any error or wrong params sended by client, they will see error page below, but server will work  
further;   

When we launched our application, tomcat will start and port set in application.properties server.port=8080 (default 8080);  

Config file with pyramid properties lays in resources and calls pyramid.properties.   

** Tools: **  
JDK 8, Spring boot 1.3.3, JSP, JQuery, Twitter Bootstrap, Maven , Git /Bitbucket, Tomcat, IntelliJIDEA 15.

--
**Rustam Akhmetov**